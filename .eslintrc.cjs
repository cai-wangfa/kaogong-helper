module.exports = {
  env: {
    browser: true,
    es2021: true,
  },

  extends: [
    'eslint:recommended',
    'plugin:@typescript-eslint/recommended',
    'plugin:vue/vue3-recommended',
    'plugin:prettier/recommended',
  ],
  overrides: [
    {
      env: {
        node: true,
      },
      files: ['.eslintrc.{js,cjs}'],
      parserOptions: {
        sourceType: 'script',
      },
    },
  ],
  parserOptions: {
    ecmaVersion: 'latest',
    parser: '@typescript-eslint/parser',
    sourceType: 'module',
  },
  plugins: ['@typescript-eslint', 'vue'],
  rules: {
    'vue/require-default-prop': 0,
    'vue/html-self-closing': 0,
    'vue/multi-word-component-names': 0,
    '@typescript-eslint/no-explicit-any': 0,
    'vue/no-mutating-props': 0,
    'no-undef': 0,
    'no-restricted-imports': [
      'error',
      {
        paths: [
          {
            name: 'lodash',
            message: '不要使用 lodash,请使用 lodash-es 作为替代',
          },
        ],
      },
    ],
  },
}
