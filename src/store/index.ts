import type { App } from 'vue'
import { createPinia } from 'pinia'

export * from './modules/compute/useComputeStore'

export * from './modules/data-analysis'

const pinia = createPinia()

/**
 * 设置pinia
 */
export function setupPinia(app: App) {
  app.use(pinia)
}

export default pinia
