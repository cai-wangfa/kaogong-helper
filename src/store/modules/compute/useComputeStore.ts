import { defineStore } from 'pinia'

import { ref } from 'vue'

export const useComputeStore = defineStore('useComputeStore', () => {
  const topic = ref<{
    config?: TopicObjChildren
    generateTopicNumber?: number
    correctness?: string | number
  }>({
    generateTopicNumber: 10,
  })

  return {
    topic,
  }
})
