import { get } from '@/utils/http/request'

/**
 * 获取成语列表
 */
export const getIdiomList = () => {
  return get<R<IdiomListType[]>>('/wx/kgIdioms/list')
}
