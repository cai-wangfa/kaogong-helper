/// <reference types="vite/client" />

/**
 * 成语列表类型
 */
interface IdiomListType {
  id: number
  derivation: string
  example: string
  explanation: string
  pinyin: string
  word: string
  abbreviation: string
}
