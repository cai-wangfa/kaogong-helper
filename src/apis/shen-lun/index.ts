export * from './norms-words'

export * from './norms-words'
export type * from './norms-words-type'

import * as personDeeds from './person-deeds-type'

export { personDeeds }
