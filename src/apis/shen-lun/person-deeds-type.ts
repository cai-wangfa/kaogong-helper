export interface PersonDeeds {
  id: number
  name: string
  description: string
  status: number
}
