import { get } from '@/utils/http/request'
import type * as Type from './person-deeds-type'
export type * from './person-deeds-type'

/**
 * 人物素材 关键词搜索
 */
export const search = (data: { keyword: string }) => {
  return get<R<Type.PersonDeeds[]>>('/kgSlPersonDeeds/search', data)
}

/**
 * 人物素材 详情
 */
export const detail = (data: { id: number }) => {
  return get<R<Type.PersonDeeds>>('/kgSlPersonDeeds/detail', data)
}

/**
 * 获取所有人物素材列表
 */
export const getAllList = () => {
  return get<R<Type.PersonDeeds[]>>('/kgSlPersonDeeds/all/list')
}
