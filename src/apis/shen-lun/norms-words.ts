import { get } from '@/utils/http/request'
import type * as Type from './norms-words-type'

/**
 * 获取申论规范词主题分类
 */
export const getKgSlNormsWordsTypeList = () => {
  return get<
    R<
      Array<{
        id: number
        name: string
      }>
    >
  >('/kgSlNormsWordsType/list')
}

export const getWeekList = () => {
  return get<R<Type.ShenLunNormsWords[]>>('/kgSlNormsWord/week/list')
}

/**
 * 通过id获取详情
 */
export const getDetail = (data: { id: number }) => {
  return get<R<Type.ShenLunNormsWords>>('/kgSlNormsWord/detail', data)
}

/**
 * 通过id获取详情
 */
export const selectByThemeId = (data: { topicId: number }) => {
  return get<R<Type.ShenLunNormsWords>>('/kgSlNormsWord/filter/list', data)
}
