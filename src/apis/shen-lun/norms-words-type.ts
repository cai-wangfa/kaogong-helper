export interface ShenLunNormsWords {
  id: number
  topic: number
  topicName: string
  context: Context[]
  createTime: string
  author: string
}

interface Context {
  id: string
  original: string
  norms: string
}
