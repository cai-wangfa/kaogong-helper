import TopicKeyboard from './keyboard/src/index.vue'
import MathMl from './math/math-ml.vue'
import TopicNav from './topic-nav/src/top-nav.vue'
import TopicResult from './topic-result/src/result.vue'

export const com = { TopicKeyboard, MathMl, TopicNav, TopicResult }

export default com
