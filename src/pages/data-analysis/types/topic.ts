export interface TopicConfig {
  type: 'bj' | 'js'
  title: string
  des: string
  formula?: string
  deviation?: number
  generateTopicNumber?: number
  generateTopic: () => GenerateTopicType
  computed?: (e: any) => boolean
}

type GenerateTopicType = {
  topic: string
  answer: string | number
  youAnswer?: string | number
  deviation?: number
  ok?: boolean
}
