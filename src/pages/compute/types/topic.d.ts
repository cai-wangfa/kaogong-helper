/// <reference types="vite/client" />

interface TopicObj {
  title: string
  mode?: string
  tip?: string
  children: Array<TopicObjChildren>
}

type GenerateTopicType = {
  topic: string
  answer: string | number
  youAnswer?: string | number
  deviation?: number
  ok?: boolean
}

interface TopicObjChildren {
  title: string
  type: string
  tip?: string
  deviation?: number
  generateTopicNumber?: number
  generateTopic?: () => GenerateTopicType
}
