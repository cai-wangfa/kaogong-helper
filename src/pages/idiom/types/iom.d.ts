/// <reference types="vite/client" />

interface IomList {
  derivation: string
  example: string
  explanation: string
  pinyin: string
  word: string
  abbreviation: string
  isStudy: boolean
}
