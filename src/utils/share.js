export default {
  data() {
    return {
      //设置默认的分享参数
      share: {
        title: '行测上岸小助手',
        path: '/pages/home/index',
        imageUrl: '',
        desc: '专注学习分享',
        content: '',
      },
    }
  },
  onShareAppMessage() {
    return {
      title: this.share.title,
      path: this.share.path,
      imageUrl: this.share.imageUrl,
      desc: this.share.desc,
      content: this.share.content,
      success() {
        uni.showToast({
          title: '分享成功',
        })
      },
      fail() {
        uni.showToast({
          title: '分享失败',
          icon: 'none',
        })
      },
    }
  },
  onShareTimeline() {},
}
