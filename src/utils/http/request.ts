const dev = false

let baseURL = ''
if (dev) {
  baseURL = 'http://127.0.0.1:3002'
} else {
  baseURL = 'https://kaogong.ruanjian211.top'
}

const baseHeader = {
  //Authorization: '123',
}

export const get = <T>(path: string, params?: any): Promise<T> => {
  return new Promise((resolve, reject) => {
    uni.showLoading()
    uni.request({
      method: 'GET',
      url: baseURL + path,
      data: params,
      header: {
        ...baseHeader,
      },
      success(res) {
        resolve(res.data as any)
        uni.hideLoading()
      },
      fail(error) {
        reject(error)
        uni.hideLoading()
      },
    })
  })
}

export const post = <T = unknown>(path: string, params: any): Promise<T> => {
  return new Promise((resolve, reject) => {
    uni.showLoading()
    uni.request({
      method: 'POST',
      url: baseURL + path,
      data: params,
      header: {
        ...baseHeader,
      },
      success(res) {
        resolve(res.data as any)
        uni.hideLoading()
      },
      fail(error) {
        reject(error)
        uni.hideLoading()
      },
    })
  })
}
