/// <reference types="vite/client" />
/**
 * @description 分页数据
 *
 */
interface Page<T> {
  total: number
  list: T[]
  pageNum: number
  pageSize: number
  pages: number
  size: number
}

/**
 * @description 响应结果
 */
interface Res<T> {
  code: number
  msg: string
  status: boolean
  data: T
}

type R<T> = Res<T>
type P<T> = Res<Page<T>>
