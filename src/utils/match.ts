export const deviationMatch = (
  targe: number,
  value: number,
  deviation: number,
) => {
  //计算误差范围
  const wcValue1 = value * (1 + deviation)
  const wcValue2 = value * (1 - deviation)

  //是否在误差范围
  if (targe >= wcValue2 && targe <= wcValue1) {
    return true
  }
  return false
}

export function generateRandomNumbersWithError(e: {
  baseValue?: number
  minError?: number
  maxError?: number
}) {
  const { baseValue, minError, maxError } = {
    baseValue: 100,
    minError: 0.02,
    maxError: 0.2,
    ...e,
  }

  // 生成第一个误差百分比
  const error1 = Math.random() * (maxError - minError) + minError
  // 生成第二个误差百分比（为了不同，可以稍微调整随机数的生成）
  const error2 =
    Math.random() * (maxError - minError) +
    minError +
    (Math.random() > 0.5 ? 0.001 : -0.001) // 稍微偏移以避免完全相同的值

  // 计算带有误差的最终值
  let value1 = baseValue * (1 + error1)
  let value2 = baseValue * (1 + error2)

  // 确保值在合理范围内（由于浮点数的精度问题，可能需要四舍五入）
  value1 = Math.round(value1 * 100) / 100
  value2 = Math.round(value2 * 100) / 100

  return { value1, value2 }
}
