import { onUnmounted } from 'vue'

/**
 * @description: 导出useEventbus
 */
export const useEventbus = () => {
  const emit = <T>(key: string, data: T) => {
    uni.$emit(key, data)
  }

  const once = <T>(key: string, callback: (data: T) => void) => {
    uni.$once(key, (e: any) => {
      callback(e)
    })
  }

  return {
    emit,
    once,
  }
}
