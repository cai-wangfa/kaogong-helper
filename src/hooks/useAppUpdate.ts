/**
 * @description: 小程序更新
 */
export const useAppUpdate = () => {
  // #ifdef MP-WEIXIN
  const updateManager = uni.getUpdateManager()

  updateManager.onCheckForUpdate((res) => {
    if (res && res.hasUpdate) {
      // 有新版本，优先
      uni.showLoading({
        title: '小程序更新中',
      })
    }
    console.log('检查是否有新版本', res.hasUpdate)
  })

  // 更新提示
  updateManager.onUpdateReady(() => {
    uni.hideLoading()
    uni.showModal({
      title: '更新提示',
      content: '新版本已经准备好，是否重启应用？',
      success(res) {
        if (res.confirm) {
          // 新的版本已经下载好，调用 applyUpdate 应用新版本并重启
          updateManager.applyUpdate()
        }
      },
    })
  })

  // 更新失败
  updateManager.onUpdateFailed(() => {
    // 新的版本下载失败
    uni.hideLoading()
    uni.showModal({
      title: '提示',
      content: '更新应用失败',
      showCancel: false,
      success: (res) => {
        if (res.confirm) {
          //更新失败，继续
          //this.checkPcLogin(query)
        }
      },
    })
  })

  // #endif

  return {}
}
