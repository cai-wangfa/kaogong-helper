import { createSSRApp } from 'vue'
import App from './App.vue'
import './styles/style.css'
import { setupPinia } from './store'

import share from '@/utils/share.js'

import setupComponent from '@/plugins/component'

export function createApp() {
  const app = createSSRApp(App)

  app.use(setupComponent)

  app.mixin(share)

  setupPinia(app)
  return {
    app,
  }
}
