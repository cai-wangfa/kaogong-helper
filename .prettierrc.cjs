module.exports = {
  // 行位不需要分号
  semi: false,
  // 使用单引号
  singleQuote: true,
  // 让函数(名)和后面的括号之间加个空格
  'javascript.format.insertSpaceBeforeFunctionParenthesis': true,
  // 让vue中的js按编辑器自带的ts格式进行格式化
  'vetur.format.defaultFormatter.js': 'vscode-typescript',
  // 让prettier使用eslint的代码格式进行校验
  eslintIntegration: true,
  // 换行符使用 auto
  endOfLine: 'auto',
}
